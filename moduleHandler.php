<?php
// si le module n'est pas pr�sent, on l'ajoute
if(!file_exists(_PS_CLASS_DIR_.'module/CharlyModule.php')||!file_exists(_PS_CLASS_DIR_.'module/BaseCharlyModule.php')){
  copy(dirname(__FILE__)."/CharlyModule.php",_PS_CLASS_DIR_.'module/CharlyModule.php'); // we copy the master class in the right dir
  copy(dirname(__FILE__)."/BaseCharlyModule.php",_PS_CLASS_DIR_.'module/BaseCharlyModule.php'); // we copy the master class in the right dir
  unlink(_PS_CACHE_DIR_.'class_index.php'); // we clear the class cache ! 
  include_once(_PS_CLASS_DIR_.'module/BaseCharlyModule.php');
  include_once(_PS_CLASS_DIR_.'module/CharlyModule.php');
} else {
  //on v�rifie que la version qu'on a avec le module n'est pas plus r�cente que celle propos�e
  $local_m = file_get_contents(dirname(__FILE__)."/BaseCharlyModule.php");
  preg_match("/\* @version ([0-9\.]+)/",$local_m,$res_loc);
  $machine_m = file_get_contents(_PS_CLASS_DIR_.'module/BaseCharlyModule.php');
  preg_match("/\* @version ([0-9\.]+)/",$machine_m,$res_mac);
  if((float)$res_mac[1] < (float)$res_loc[1]){
    copy(dirname(__FILE__)."/BaseCharlyModule.php",_PS_CLASS_DIR_.'module/BaseCharlyModule.php');
  }
  unset($local_m,$machine_m,$res_loc,$res_mac);
}

?>