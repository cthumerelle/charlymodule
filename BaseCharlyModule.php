<?php
/**
 * @author charly - 2014 *
 * @version 1.0
 */
class BaseCharlyModule extends Module {

	public function __construct() {
    parent::__construct();
    $this->settings = array();
    $this->context = Context::getContext();
  }
  
  public function install() {
    $ok = true;
    foreach($this->getSettings() as $name => $setting){
     if(isset($setting['default'])){
       $ok = $ok && $this->confSet($name,$setting['default']);
     }
    }
    
    $ok = $ok && parent::install();
    
    if(isset($this->hooks)){
      foreach($this->hooks as $hook){
        $ok = $ok && $this->registerHook($hook);
      }
    }
    
    if(method_exists($this,'installDb')){
      $ok = $ok && $this->installDb();
    }
    
    return $ok;
  }

  public function uninstall() {
    $ok = true;
    foreach($this->getSettings() as $key=>$setting)
    {
      $ok = $ok && Configuration::deleteByName($this->prefix.$key);
      if(isset($setting['need_activation'])) {
        $ok = $ok && Configuration::deleteByName($this->prefix.$key.'_A');
      }
    }
    
    return $ok && parent::uninstall();
  }
  public function getContent(){

    $output = '<h2>'. $this->displayName .'</h2>';
    // updating configuration
    if  ( Tools::isSubmit( $this->submitName )) {
      //we deactivate all checkboxes
      foreach($this->settings as $key=>$setting)
      {
        if($setting['type'] == 'checkbox') {
          $result = $this->confSet($key, 0);
        }
        if(isset($setting['need_activation']) && $setting['need_activation'] == true) {
          $result = $this->confSet($key.'_A', 0);
        }
      }

      $values = array();
      foreach($_POST as $k=>$v)
      {
        if(strpos($k,$this->prefix)!==FALSE){
          $this->confSet($k, $v);
        }
      }
      $output .= '<div class="conf confirm">' . $this->l('Settings updated') . '</div>'."\n";
    }

    // display configuration form
    return $output . $this->displayForm();
  }

  /**
   * display setting Form
   */
  public function displayForm() {
    $output = '<form action="' .htmlentities($_SERVER['REQUEST_URI']). '" method="post">';
    
    //settings options
    if(isset($this->settings[0]) && gettype($this->settings[0])=="array"){
      foreach($this->settings as $key => $value){
        if($key != ''){
          $output.=	'<fieldset class="space"><legend><img src="../modules/'.$this->name.'/logo.gif" />' . $key . '</legend>';
          foreach($value as $key=>$setting)
          {
            $output .= $this->displaySetting($key,$setting);
          }
          $output.='<div class="margin-form"><input type="submit" name="'.$this->submitName.'" value="' . $this->l('Update Parameters') . '" class="button" /></div>';
          $output.='</fieldset>';
        } else {
          $output.=	'<fieldset class="space"><legend><img src="../modules/'.$this->name.'/'.(isset($value['icon'])?$value['icon']:'logo.gif').'" />' . (isset($value['label'])?$value['label']:$this->l('Settings')) . '</legend>';
          foreach($value['settings'] as $key=>$setting)
          {
            $output .= $this->displaySetting($key,$setting);
          }
          $output.='<div class="margin-form"><input type="submit" name="'.$this->submitName.'" value="' . $this->l('Update Parameters') . '" class="button" /></div>';
          $output.='</fieldset>';
        }
      }
    } else {    
      $output.=	'<fieldset class="space"><legend><img src="../modules/'.$this->name.'/logo.gif" />' . $this->l('Settings') . '</legend>';
      foreach($this->settings as $key=>$setting)
      {
        $output .= $this->displaySetting($key,$setting);
      }
      $output.='<div class="margin-form"><input type="submit" name="'.$this->submitName.'" value="' . $this->l('Update Parameters') . '" class="button" /></div>';
      $output.='</fieldset>';
    }
    
    $output.='</form>';
    return $output;
  }

  /**
   * select input type
   * @param string $key
   * @param string $setting
   * @return string
   */
  public function displaySetting($key,$setting) {

    $out = '<label>' . $setting['label'] . '</label>';
    $out.= '<div class="margin-form" >';
    if (isset($setting['need_activation']) && $setting['need_activation']){
      $out.='<input type="checkbox" name="' . $key . '_A" value="1" ' . ((static::getValue($key.'_A', $this->confGet($key.'_A')))?'checked="checked"':'') . '" style="float:left;margin-right:5px;"/>';
    }
    if ($setting['type'] == 'text'){
			$out.='<input type="text" name="' . $key . '" id="' . $key . '" value="' . (isset($_POST[$key])?static::getValue($key):$this->confGet($key)) . '" size="' . (isset($setting['size'])?$setting['size']:'') . '"/>';
    } elseif ($setting['type'] == 'textarea'){
      $out.='<textarea cols=80 rows=5 name="' . $key . '" id="' . $key . '" >' . (isset($_POST[$key])?static::getValue($key):$this->confGet($key)) . '</textarea>';
    } elseif ($setting['type'] == 'checkbox'){
      $out.='<input type="checkbox" name="' . $key . '" id="' . $key . '" value="1" ' . ((static::getValue($key, $this->confGet($key)))?'checked="checked"':'') . '" style="float:left;margin-right:5px;" />';
    } elseif ($setting['type'] == 'radio'){
      $options = isset($setting['options'])?$setting['options']:array('1'=>$this->l('yes'),'0'=>$this->l('no'));
      $out.='<div style="float:left;">';
      foreach($options as $value=>$option)
        $out.='<input type="radio" name="' . $key . '" id="' . $key . '_' . $value . '" value="' . $value . '" ' . ((static::getValue($key, $this->confGet($key))==$value)?'checked="checked"':'') . '" />' . '<label class="t" for="' . $key . '_' . $value.'">&nbsp;' . $option . '</label>' . '<br/>';
      $out.='</div><br class="clear"/>';
    }
    $out.=(isset($setting['infos']) || (isset($setting['need_activation']) && $setting['need_activation']))?'<p> ' . ((isset($setting['need_activation']) && $setting['need_activation'])?$this->l('[check to activate] : '):'') . (isset($setting['infos'])?$setting['infos']:'') . '</p>':'';
    $out.='</div>';
		$out.='<br class="clear" />'."\n";

    return $out;
  }
  
  public static function getValue($key, $default_value = false){
		if (!isset($key) || empty($key) || !is_string($key))
			return false;
		$ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $default_value));
		return $ret;
	}
  
  public function confGet($key){
    return Configuration::get($this->prefix.$key);
  }
  public function confSet($key,$value){
    return Configuration::updateValue($this->prefix.$key,$value);
  }
  public function getSettings(){
    $settings = array();
    if(isset($this->settings[0]) && gettype($this->settings[0])=="array"){
      foreach($this->settings as $key => $value){
        if($key != ''){
          $settings = array_merge($settings, $value);
        } else {
          $settings = array_merge($settings, $value['settings']);
        }
      }
    } else {    
      $settings = $this->settings;
    }
    return $settings;
  }
}
